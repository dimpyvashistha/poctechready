package WebTests;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import WebUtils.LogResults;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.logging.Level;

public class BaseClass {

    public static WebDriver driver ;
    protected ExtentReports extent;
    WebDriver currentDriver;
    URL url = null;
    URL appiumUrl = null;
    String appFile = null;
    String driverPath =  "./src/test/resources/drivers/";
    String extension = ".exe";


    @BeforeSuite(alwaysRun = true)
    public void setup() throws Exception {
        System.setProperty("webdriver.chrome.driver", new File(driverPath + "chromedriver" + extension).getCanonicalPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        //options.addArguments("disable-extensions");
        options.addArguments("--disable-notifications");
        options.setCapability( "goog:loggingPrefs", getLogPrefs());
        HashMap<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.media_stream_mic", 1);
        options.setExperimentalOption("prefs", prefs);
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE); //To avoid NoAlerTException by changing default behaviour of the driver
        currentDriver = new ChromeDriver(options);
        //LogResults.logStep().log(LogStatus.INFO, "Open Browser ", "   Chrome");
        currentDriver.manage().window().setSize(new Dimension(1024, 768));
        currentDriver.manage().window().maximize();
        currentDriver.manage().deleteAllCookies();
        driver = currentDriver;
    }

    /**
     * Perform the actions with each and every method.
     * 
     * @param result
     */
    //@AfterMethod(alwaysRun = true)
    protected void afterMethod(ITestResult result) {
        DateTimeFormatter dates = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            if (result.getStatus() == ITestResult.FAILURE) {
                LogResults.logStep().log(LogStatus.FAIL, "Test Status", result.getThrowable());
            } else if (result.getStatus() == ITestResult.SKIP) {
                LogResults.logStep().log(LogStatus.SKIP, "Test Status", "Test skipped " + result.getThrowable().getMessage());
            } else {
                LogResults.logStep().log(LogStatus.PASS, "Test Status", "Test passed");
            }
            LogResults.getReporter(this.getClass().getSimpleName()).endTest(LogResults.logStep());
            LogResults.getReporter(this.getClass().getSimpleName()).flush();
            System.out.println("Ending Test: "+ result.getName() + " Time: " +  dates.format(now));
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method returns the LoggingPreferences
     */
    public static LoggingPreferences getLogPrefs() {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable( LogType.PERFORMANCE, Level.ALL );
        return logPrefs;
    }
}