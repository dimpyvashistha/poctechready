package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class DashboardPage {

    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public DashboardPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Get the First Name from Dashboard
     *
     * @author dimpyv
     * @return String
     */
    public String getFirstNameFromDashboard(){
        return txtName.getText();
    }

    //WebElements
    @FindBy(xpath = "//span[@style='text-transform:capitalize']")
    WebElement txtName;
    @FindBy(xpath = "//input[@name='password']")
    WebElement txtPassword;
    @FindBy(xpath = "//a[@class='theme-btn theme-btn-small theme-btn-transparent ml-1 waves-effect']")
    WebElement btnLogin;
}


