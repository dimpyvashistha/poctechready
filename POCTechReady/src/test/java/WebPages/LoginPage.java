package WebPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import WebUtils.ElementImplement;


public class LoginPage {
    
    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();
    
    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Enter UserName and Password for sign in
     * @param email
     * @param password
     * @author dimpyv
     */
    public void enterUserNamePassword(String email, String password){
        webElementOperations.writeText(driver,1000,txtEmail,email);
        webElementOperations.writeText(driver,1000,txtPassword,password);
    }

    /**
     * Click on Login Button
     *
     * @author dimpyv
     */
    public void clickLoginButton() {
        webElementOperations.click(driver,1000,btnLogin);
    }


    //WebElements
    @FindBy(xpath = "//input[@name='email']")
    WebElement txtEmail;
    @FindBy(xpath = "//input[@name='password']")
    WebElement txtPassword;
    @FindBy(xpath = "//button[@class='btn btn-default btn-lg btn-block effect ladda-button waves-effect']")
    WebElement btnLogin;
}


